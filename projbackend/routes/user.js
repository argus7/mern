const express = require("express");
const router = express.Router();
const {getUserByID, getUser,getAllUser, updateUser,userPurchasedList} = require("../controllers/user");
const {isAuthenticated, isSignedIn, isAdmin} = require("../controllers/auth");
router.param("userId",getUserByID)

router.get("/user/:userId",isSignedIn, isAuthenticated, getUser)
router.get("/users",getAllUser)
router.put("/user/:userId",isSignedIn, isAuthenticated, updateUser);
router.put("/user/:userId",isSignedIn, isAuthenticated, userPurchasedList);

module.exports = router