var express = require('express');
const { check } = require('express-validator');

const {signup, signout, signin, isSignedIn} = require("../controllers/auth");
var router = express.Router();
router.post("/signup",[
    check("name", "Name should be atleast of 4 char").isLength({min: 5}),
    check("email","Email is required").isEmail(),
], signup);

router.post("/signin",[
    check("email","Please enter valida email").isEmail(),
    check('password',"Please enter a valid password").isLength({ min: 5 }),

], signin);
router.get("/signout",signout);

router.get("/testroute", isSignedIn, (req, res) => {
    res.json(req.token) 
})

module.exports = router;