const User = require("../models/user")
var jwttoken = require('jsonwebtoken');
var expressjwt = require('express-jwt');

const {body, validationResult} = require('express-validator');


exports.signup = (req, res) => {
    const errors = validationResult(req)
    if(!errors.isEmpty()){
        return res.status(422).json({
            error: errors.array()[0].msg
        })
    }
    const user = new User(req.body);
    user.save((err, user) => {
        if (err) {
            return res.status(400).json({
                message: "Not able to create user",
                error: err
            })
        }
        return res.json({
            name: user.name,
            email: user.email,
            userid: user._id,
            full_name: user.full_name
        });
    })
}

exports.signout = (req, res) => {  
    // Clearing the cookie
    const cookied = res.cookie('token')
    console.log(cookied, "is the cookies")
    res.clearCookie('token');   
     res.json({
        message : "User signout successfully"
    })
}


exports.signin = (req, res) => {
    const errors = validationResult(req);
    const { email, password } = req.body;
  
    if (!errors.isEmpty()) {
      return res.status(422).json({
        error: errors.array()[0].msg
      });
    }
  
    User.findOne({ email }, (err, user) => {
      if (err || !user) {
        return res.status(400).json({
          error: "USER email does not exists"
        });
      }
  
      if (!user.authenticate(password)) {
        return res.status(401).json({
          error: "Email and password do not match"
        });
      }
  
      //create token
      const token = jwttoken.sign({ _id: user._id }, process.env.SECRETCODE);
      //put token in cookie
      res.cookie("token", token, { expire: new Date() + 9999 });
  
      //send response to front end
      const { _id, name, email, role } = user;
      return res.json({ token, user: { _id, name, email, role } });
    });
  };
  


//Protected route
exports.isSignedIn = expressjwt({
    secret: process.env.SECRETCODE,
    userProperty: "token"
}); 


//custom miidle ware

exports.isAuthenticated = (req, res, next) => {
    let checker = req.profile && req.token && req.profile._id == req.token._id
    if(!checker){
        res.json({
            error: "Action denied"
        })
    }
    next();
}
exports.isAdmin = (req, res, next) => {
    if(req.profile === 0) {
        return res.status(403).json({
            error: "Action denied"
        })
    }
    next();
}