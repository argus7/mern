
const User = require("../models/user")
// const Order = require("../models/order")
const {cart, productCart} = require("../models/order")

//Method to getuser by user id created by mongoose id
exports.getUserByID = (req, res, next, id) => {
    User.findById(id).exec((err, user) => {
        if(err || !user){
            return res.status(400).json({
                error: "no user found"
            })
        }
        req.profile = user;
        next();
    });
};
//Get all the user in the form of list can do validation with admin and normal user later or can make subscribed user later
exports.getAllUser = (req, res) => {
    User.find().exec((err, user) => {
        if(err || !user){
            return res.status(400).json({
                error: "no user found"
            })
        }
        res.json(user);
    })
}
//Method to update details of the user, currently user can update all the field as it is not validated else we can validate for name and last name only 
exports.updateUser = (req, res) => {
    User.findByIdAndUpdate(
      { _id: req.profile._id },
      { $set: req.body },
      { new: true, useFindAndModify: false },
      (err, user) => {
        if (err) {
          return res.status(400).json({
            error: "You are not authorized to update this user"
          });
        }
        user.salt = undefined;
        user.encry_password = undefined;
        res.json(user);
      }
    );
};
// to do:
exports.userPurchasedList = (res, req) => {
    cart.find().purchases
}
exports.getUser = (req, res) => {
    req.profile.salt = undefined
    req.profile.salt = undefined

    return res.json(req.profile)
}