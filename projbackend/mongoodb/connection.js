const mongoose = require("mongoose")
require('dotenv').config()

//Connect mongoose db locally
//DB CONNECTION
mongoose
  .connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => {
    console.log("DB CONNECTED");
  }).catch((reason) => {
    console.log(`DATABASE NOT FOUND ${reason}`);
  });