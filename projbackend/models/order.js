const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const ProductCartSchema = new mongoose.Schema({
    prodcut:{
        type: ObjectId,
        ref: "Product"
    },
    name: String,
    count: Number,
    price: Number
});


const CartSchema = new mongoose.Schema({

    products: [ProductCartSchema],
    transaction_id: {

    },
    amount: {type: Number},
    address: String,
    updated: Date,
    user :{
        type: ObjectId,
        ref: "User"
    }
},{timestamps: true})

const cart = mongoose.model("Cart", CartSchema)
const productCart = mongoose.model("ProductCart", ProductCartSchema)

module.exports = {cart, productCart}
