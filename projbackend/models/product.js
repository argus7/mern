const mongoose = require('mongoose');
const {ObjectID} = mongoose.Schema
const ProductSchema = mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        maxlenght: 32
    },
    description:{
        type: String,
        trim: true,
        required: true,
        maxlenght: 2000
    },
    price:{
        type: Number,
        required: true,
        maxlength: 32,
        trim: true 
    },
    category:{
        type: ObjectID,
        ref: "Category",
        required: true
    },
    stock:{
        type: Number,
    },
    sold:{
        type: Number,
        default: 0
    },
    seller:{
        name: String
    },
    
    photo: {
        type: Buffer,
        contentType: String
    },
    size:{
        type: Number,
        contentType: String
    }
}, {timestamps: true})

module.exports = mongoose.model("Product",ProductSchema)

