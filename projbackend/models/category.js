var mongoose =   require('mongoose');

const CategorySchema = new mongoose.Schema({
    name:{
        type: String,
        trim: true,
        required: true,
        maxlength: 32,
        uniquw: 32
    }
}, {timestamps:true}
);

module.exports = mongoose.model("Category",CategorySchema);