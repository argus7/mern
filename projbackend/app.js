const connection = require("./mongoodb/connection")
const express = require("express");

// Middle ware :  Body parser, cookie parser and cors
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")
const cors = require("cors")

//creating app 
const app = express();

//creating My routes
const authRoutes = require("./routes/auth")
const userRoutes = require("./routes/user")
//Connect mongoose db locally
  //Get port from environment file
  //PORT
const port = process.env.PORT || 8000;

// Start working with middle ware
app.use(bodyParser.json()) // body parser parse application/json
app.use(cookieParser());
app.use(cors());

//ROUTES
app.use("/api", authRoutes)
app.use("/api", userRoutes)
// Start are the port
app.listen(port, () => {
  console.log(`app is running at ${port}`)
});