const express = require("express")
const app = express()


// define a port

const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get("/login", (req, res) => {
    res.send("On login")
})
app.get("/registration", (req, res) => {
    res.send("On resistration")
})
const admin = (req, res) => {
  res.send("This is admin");
}

const isAdmin = (req, res, next) => {
  console.log("Is the adming running");
  next();
}
const isLoggedIn = (req, res, next) => {
  console.log("checking admin is logged in or not")
  next();
}
app.get("/admin",isLoggedIn, isAdmin, admin)
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})